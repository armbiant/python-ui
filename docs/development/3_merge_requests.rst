Merge requests
================

Please use the `Merge Request <https://gitlab.com/pythonLabor/labpyui/merge_requests>`_ section
of our repository to submit merge request.
In case you are not ready, please mark it with 'WIP' (work in progress) - this prevents accidental merging.
