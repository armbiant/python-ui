LabPyUi
=======

LabPyUi is a high level graphical user interface library for rapid
generation of control elements of laboratory devices, instruments and
synthesizers. It also contains elements for logging and plotting. It is
based on python3/PySide2-Qt5, including tight interfacing to SiLA2
(sila-standard.org).

LabPyUi offer ready-made modules that makes it very convenient to build
new interfaces to lab devices and instruments.

In contrast to concurrent systems from large instrument software
vendors, like e.g. NI LabView (R), LabPyUi is very lean and has an
incredibly small footprint.

With its tight connectiviy and interoperability with the open lab
communication standard `SiLA2 <https://sila-staandard.org>`__ through
their `SiLA\_python library <https://gitlab.com/sila2/sila_python>`__
many standardised services will not even require a single line of extra
code to control a SiLA2 based device or service.

LabPyUi main features
---------------------

--------------

-  unified GUI environment for easily creating lab device interfaces
   (python3/QT5/pyside2 based)
-  complete application with advanced menus, toolbars, statusbar,
   settings dialogues
-  dock-able windows, flexible window movement, saving of window states
-  settings will be preserved over closing the app
-  command-line parsing
-  signals for communication, threading for multi-threading applications
-  many standard widgets (temperature display, timer, alphanumeric
   displays, data plotting)
-  pluggable widget system
-  `SiLA2 <https://sila-staandard.org>`__ support
-  Arduino and Raspberry Pi support
-  extremely small footprint, fast
-  examples and demo library

LabPyUi packages
----------------

--------------

-  core - LabPyUi Application and MainWindow or Workbench
-  widgets - collection of reusable widgets
-  math\_models - mathematical models for data evaluation and simulation

LabPyUi PyPi installation
-------------------------

--------------

the fastest way to install LabPyUi is through PyPi and pip
(unfortunately this does not currently work on RaspberryOS)

::

    # --user indicatates to do a local installation into home directory
    pip3 install --user labpyui

LabPyUi installation with labpyui\_install.py
---------------------------------------------

--------------

::

    wget https://gitlab.com/pythonLabor/LabPyUi/labpyui_install.py

    #git clone https://gitlab.com/pythonLabor/LabPyUi.git

    cd labpyui

    # this installs everything in a virtual environment for testing:

    python3 labpyui_install.py 

    (Note: please also install dependencies !!)

quick start demos
-----------------

--------------

see examples/demos for feature demos

more examles can be find in the examples section

documentation
-------------

--------------

see docs/quickstart.rst guide for a quick start

and docs for complete documentation

generating the documentation
----------------------------

current widgets
---------------

--------------

-  alpha-numeric
