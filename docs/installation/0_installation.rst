Installing LabPyUi
=======================

.. include:: installation_linux.rst

.. include:: installation_raspberry_pi.rst

.. include:: installation_windows

.. include:: installation_manual


Installation
_____________

To run the code in this repository it is highly recommended to use
*Python 3.7* or *Python 3.8*.

*Note*: This code is developed under Ubuntu. It should work with
other operating systems, but has not been tested extensively yet.

To install the library and development tools an automatic setup routine
exists. Alternatively a manual installation is possible.

Prerequisites for the installation are:

- **Python3**

  - |linux| Linux _: Oftentimes ``python3`` is included in the base installation, otherwise run ``sudo apt-get install python3``
  - |windows| Windows _: Use a python 3.6 or 3.7 implementation (e.g. `CPython <https://www.python.org/downloads/>`__)
              !! the *grcpio library* for python 3.8 is not available as precompiled binary for MSWindows - status: 19/11 

- **pip (package manager for Python)**

  Pythons package manager is used to automatically download and install the packages necessary to run the SiLA client and server applications. This requires a working internet connection. Installation routines:

  - |linux| Linux _: ``sudo apt-get install pip3``
  - |windows| Windows: Should be included in the implementations installer

- **git (recommended)**

  Git is used to download this repositories contents. If you are not familiar with git, GitLab has a documentation on `how to get started <https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html>`__. Installation routines:

  - |linux| Linux _: ``sudo apt-get install git``
  - |windows| Windows _: Either find your favorite GUI, or install the `command line version <https://git-scm.com/download/win>`__.

  Once you have installed git, clone this repository

.. code-block:: console

     # create a directory for the source code and change into it
     mkdir labpyui & cd labpyui
     # clone into the current directory
     git clone https://gitlab.com/pythonlabor/labpyui.git .

Otherwise you can download the current version on the projects `main page <https://gitlab.com/pythonLabor/labpyui>`__ using the button on the top right: |download-button|

- **A virtual environment for SiLA. (recommended)**

  It is recommended to set-up a `virtual environment <https://wiki.ubuntuusers.de/virtualenv/>`__ (venv) for the installation, at least in the development phase. While the automatic installation routine can theoretically create the environment for you, it is (as of now) more reliable to do this manually beforehand. If you have no idea what a virtual environment is, above link and possibly further literature is recommended. Remember to activate the environment before the installation (``[venv]/bin/activate`` in Linux |linux|, ``[venv]/Scripts/activate.bat`` in Windows |windows|)

Automatic (guided) installation
--------------------------------

The automatic installation can be run with the command

.. code:: bash

    python3 .py

It will guide through the complete installation, including the download
of all required packages, the installation of the SiLA2 library and
further tools. If you manually activated your virtual environment choose
``[N]`` at the corresponding question.


.. |sila-python| image:: ../images/-logo.png
.. |linux| image:: ../images/icons/linux.png
.. |windows| image:: ../images/icons/windows.png
.. |download-button| image:: ../images/installation/gitlab-download.png
